# SaysWho

A simple classic memory game. (Requires a 386+, PC speaker recommended)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## SAYSWHO.LSM

<table>
<tr><td>title</td><td>SaysWho</td></tr>
<tr><td>version</td><td>v0.2</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-07-30</td></tr>
<tr><td>description</td><td>A simple memory game.</td></tr>
<tr><td>summary</td><td>A simple classic memory game. (Requires a 386+, PC speaker recommended)</td></tr>
<tr><td>keywords</td><td>dos game</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://fd.lod.bz/repos/current/pkg-html/sayswho.html</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://gitlab.com/DangerApps/sayswho</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[BSD 3-Clause License](LICENSE)</td></tr>
</table>
